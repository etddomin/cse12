Domingo, Ethan
etddomin
Spring 2020
Lab 1

-----------
DESCRIPTION

In this lab, the user flips switches from 0 to 0, each corresponding to
logic circuit that turns specified LEDs on and off.

-----------
FILES

Lab1.lgi

This file includes the Logic circuits for the lab

-----------
INSTRUCTIONS
This program is intended to be run using the MultiMedia Logic Simulator.
Simply open the file on the simulator, and flip the allocated switches accordingly.
